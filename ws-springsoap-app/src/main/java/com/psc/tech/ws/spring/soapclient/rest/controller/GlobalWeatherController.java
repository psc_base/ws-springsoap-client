package com.psc.tech.ws.spring.soapclient.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.psc.tech.ws.spring.soapclient.rest.metadata.ReportMetatData;

import net.webservicex.GetCitiesByCountry;
import net.webservicex.GetCitiesByCountryResponse;

@RestController
@RequestMapping(value = ReportMetatData.GETWETHER)
public class GlobalWeatherController {

    @Autowired
    private WebServiceTemplate webServiceTemplate;

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
    public String getWeather() {
        GetCitiesByCountry getCitiesByCountry = new GetCitiesByCountry();
        getCitiesByCountry.setCountryName("India");
        GetCitiesByCountryResponse response = (GetCitiesByCountryResponse) webServiceTemplate.marshalSendAndReceive(
                "http://www.webservicex.net/globalweather.asmx", getCitiesByCountry,
                new SoapActionCallback("http://www.webserviceX.NET/GetCitiesByCountry"));
        System.out.println(response.getGetCitiesByCountryResult());
        return response.getGetCitiesByCountryResult();
    }

}
