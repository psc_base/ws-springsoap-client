package com.psc.tech.ws.spring.soapclient.rest.service.factory;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.pool2.KeyedPooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.springframework.stereotype.Component;

public class UnmarshallerFactory implements KeyedPooledObjectFactory<Object, PooledObject> {

    final static Map<Object, JAXBContext> JAXB_CONTEXT_MAP = new HashMap<Object, JAXBContext>();

    @SuppressWarnings("rawtypes")
    @Override
    public PooledObject makeObject(Object unmarshallerKey) throws Exception {
        if (unmarshallerKey instanceof Class) {
            Class clazz = (Class) unmarshallerKey;
            // Retrieve or create a JACBContext for this key
            JAXBContext jc = JAXB_CONTEXT_MAP.get(unmarshallerKey);
            if (jc == null) {
                try {
                    jc = JAXBContext.newInstance(clazz);
                    JAXB_CONTEXT_MAP.put(unmarshallerKey, jc); // JAXB is thread safe
                } catch (JAXBException e) {
                    return null;
                }
            }
            try {
                return (PooledObject) jc.createUnmarshaller();
            } catch (JAXBException e) {
            }
        }
        return null;
    }

    @Override
    public void activateObject(Object key, PooledObject p) throws Exception {
    }

    @Override
    public boolean validateObject(Object key, PooledObject p) {
        return true;
    }

    @Override
    public void destroyObject(Object key, PooledObject p) throws Exception {

    }

    @Override
    public void passivateObject(Object key, PooledObject p) throws Exception {
    }
}
